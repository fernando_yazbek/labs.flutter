import 'dart:async';
import 'dart:convert';

import 'package:bytebank/http/webclient.dart';
import 'package:bytebank/models/transaction.dart';
import 'package:http/http.dart';

class TransactionWebClient {
  Future<List<Transaction>> findAll() async {
    final Response response =
        await client.get(baseUrl);

    final List<dynamic> decodedJson = jsonDecode(response.body);
    return decodedJson
        .map((dynamic json) => Transaction.fromJson(json))
        .toList();
  }

  Future<Transaction> save(Transaction transaction, String password) async {
    // { 3 }

    final String transactionJson = jsonEncode(transaction.toJson());

    // await Future.delayed(Duration(seconds: 10)); // Delay para testes

    final Response response = await client.post(baseUrl,
        headers: {
          'Content-type': 'application/json',
          'password': password,
        },
        body: transactionJson);

    switch (response.statusCode) {
      case 200:
        return Transaction.fromJson(jsonDecode(response.body));
        break;

      case 400:
        throw CustomException('There was an error submitting transaction');
        break;

      case 401:
        throw CustomException('Authentication failed');
        break;
      
      case 404:
        throw CustomException('Error 404');
        break;

      case 409:
        throw CustomException('Transaction already exists');
        break;

      default:
        throw CustomException('Exception Unknown');
        break;
    }

    // { 5 }
  }

  // Transaction _toTransaction(Response response) {
  //   Map<String, dynamic> json = jsonDecode(response.body);
  //   // { 2 }
  //   return Transaction.fromJson(json);
  // }

  // List<Transaction> _toTransactions(Response response) {
  //   final List<dynamic> decodedJson = jsonDecode(response.body);

  //   // { 4 }

  //   return decodedJson
  //       .map((dynamic json) => Transaction.fromJson(json))
  //       .toList();
  // }
}

class CustomException implements Exception {
  final String message;

  CustomException(this.message);
}

// -- 1 --
//
// final Map<String, dynamic> contactJson = transactionJson['contact'];
// final Transaction transaction = Transaction(
//   transactionJson['value'],
//   Contact(
//     0,
//     contactJson['name'],
//     contactJson['accountNumber'],
//   ),
// );
// transactions.add(transaction);

// -- 2 --
//
// final Map<String, dynamic> contactJson = json['contact'];
// return Transaction(
//   json['value'],
//   Contact(
//     0,
//     contactJson['name'],
//     contactJson['accountNumber'],
//   ),
// );

// -- 3 --
//
// save:
// Map<String, dynamic> transactionMap = _toMap(transaction);
//
// Map<String, dynamic> _toMap(Transaction transaction) {
//   final Map<String, dynamic> transactionMap = {
//     'value': transaction.value,
//     'contact': {
//       'name': transaction.contact.name,
//       'accountNumber': transaction.contact.accountNumber
//     }
//   };
//   return transactionMap;
// }

// -- 4 --
//
// final List<Transaction> transactions = List();
// for (Map<String, dynamic> transactionJson in decodedJson) {
//   // { 1 }
//   transactions.add(Transaction.fromJson(transactionJson));
// }

// -- 5 --
// Outra forma de fazer (Igual ao tutorial utilizando MAP<>)
// O Switch seria substituido por um if (response.statusCode == 200)
// Logo em seguida essa função:
//
// void _throwHttpError(int statusCode) {
//   throw Exception(_statusCodeResponses[statusCode]);
// }
//
// static final Map<int, String> _statusCodeResponses = {
//   400 : 'There was an error submitting transaction',
//   401 : 'Authentication failed'
// };
